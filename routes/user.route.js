const express = require('express');
const router = express.Router();

const user_controller = require('../controllers/user.controller')

router.post('/create', user_controller.user_create);
router.post('/login', user_controller.user_login);
router.get('/:id', user_controller.user_details);
router.post('/verify_token', user_controller.verify_token);

module.exports = router;