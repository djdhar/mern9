const User = require('../models/user.model');
const jwt = require('jsonwebtoken')
const SECRET_KEY = "SECRET_KEY"
var _ = require('lodash');

exports.user_create = function (req, res) {
    let user = new User(
        {
            name: req.body.name,
            email: req.body.email,
            password: req.body.password
        }
    );

    user.save().then((user)=>{
         res.send(user)
    }).catch((err)=>{
        res.send(err)
    })
};

exports.user_login = function (req, res) {

    id = req.body.id
    password = req.body.password

    User.findById(id).then((user) => {
        if(user.password === password) {
            const token = jwt.sign(
                { user },
                SECRET_KEY,
                {
                  expiresIn: "2h",
                }
              );
            res.send({
                "message" : `${user.name} logged in successfully !!!`,
                "token": token
            })
        } else {
            res.send({
                "message" : `Invalid credentials!!!`
            })
        }
    }).catch((err)=>{
        res.send(err)
    })
};

exports.user_details = function (req, res) {
    User.findById(req.params.id).then((user)=>{
        res.json(user)
    }).catch((err)=>{
        res.send(err)
    })
};

exports.verify_token = function (req, res) {
    token = req.headers["authorization"].split(' ')[1]
    jwt.verify(token, SECRET_KEY, (err, authData) => {
        if(err) {
            res.send({message : "Invalid Token"})
        } else {
            res.send({
                message: "valid token",
                authData: _.omit({...authData.user}, ['password'])
            })
        }
    })
    res.send(token)
};
